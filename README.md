# Feladatok:

### 1. feladat: Prímszám ellenőrzés

Ellenőrizzük a konzolról beolvasott számról, hogy prímszám-e, ha igen akkor írja ki, hogy `Prím`, egyébként pedig,
hogy `Nem prím`.

### 2. feladat: Hónapok

Konzolról olvassuk be egy hónapnak a nevét és a név alapján írjuk ki, hogy hányadik hónap.

### 3. feladat: Időjárás

Kérdezzük le [https://openweathermap.org/](https://openweathermap.org/) -ról az aktuális budapesti időjárást (elég csak
a hőmérséklet). Openweather api dokumentáció: [https://openweathermap.org/current](https://openweathermap.org/current).
A lekérdezéshez szükésges api kulcsot email-ban küldtem.