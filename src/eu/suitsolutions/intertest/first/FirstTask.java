package eu.suitsolutions.intertest.first;

import java.util.Scanner;

/**
 * <h3>1. feladat: Prímszám ellenőrzés</h3>
 * Ellenőrizzük a konzolról beolvasott számról, hogy prímszám-e, ha igen akkor írja ki, hogy 'Prím', egyébként pedig, hogy 'Nem prim'.
 */
public class FirstTask {

    public static void main(String[] args) {
        var input = new Scanner(System.in);
        var prime = isPrime(input.nextInt());
        System.out.println(prime);
    }

    /**
     * Ellenőrzi a megadott számról, hogy prím-e.
     *
     * @param number a tesztelni kívánt szám.
     * @return true, ha a szám prím, egyébként false.
     */
    private static boolean isPrime(int number) {
        return false;
    }
}
